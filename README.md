# Objectifs

Ce repository contiendra les sources du nouveau site web de l'équipe Synalp qui, je l'espère, sera déployé au plus tard à l'automne 2017.

Le principe est d'utiliser un compilateur de site web statique, afin que:

- Toute personne ayant accès à gitlab.inria.fr puisse ajouter du contenu (au format Markdown) via GIT
- Le site soit automatiquement compilé et déployé (via un Runner - cf. gitlab-CI) en tant que site statique sur le serveur de site web du LORIA prévu à cet effet
- Le contenu du site actuel puisse être rapatrié sans trop d'effort sur le nouveau site

Ceci permettra de rajeunir le site actuel de l'équipe, et surtout de le transférer depuis TALC1 (qui n'est pas pérenne) vers le serveur du LORIA dédié, avec une sauvegarde assurée via le gitlab inria.

# Questions techniques

L'idée est de continuer d'adopter une solution légère, rapide, sécurisée, moderne (cf. https://jamstack.org).

L'ancien site était compilé par *Nikola*, qui n'est plus aujourd'hui très à jour.
Les alternatives qui semblent les meilleures seraient *Jekyll*, *Hugo* ou *Pelican*,
(cf. https://www.staticgen.com)
ou peut-être une sur-couche à ces compilateurs ressemblant plus à du CMS
(cf. https://headlesscms.org)
... ?

Un avis ? N'hésitez pas à réagir sur le fil de discussion: https://gitlab.inria.fr/cerisara/synalpweb/issues/1

# Solution choisie

Pelican

