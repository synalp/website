# About the new Synalp website

2017-09-04 (author: `Yannick Parmentier`)

# Table of contents
1. [Introduction](#introduction)
2. [Quickstart](#quickstart)
   1. [Architecture of the website](#archi)
   2. [Adding new content](#new)
   3. [Compiling the website](#compile)   
3. [Advanced features](#advanced)
4. [Publishing the website](#publish)
5. [References](#refs)

## <a name="introduction"></a>Introduction 

The pelican-j subdirectory contains the new team website ([version used online](http://synalp.loria.fr) as of 1st Sept. 2017).

This website is not hand-written, nor edited via an integrated [CMS](https://en.wikipedia.org/wiki/Content_management_system) (such as Joomla, WordPress or others). It is automatically compiled from a source code (the workflow is comparable to the production of a PDF document which would be compiled from a latex source code).

In the case of the Synalp website, it is written in [markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) language, and is compiled by the [pelican](https://blog.getpelican.com/) static site generator. The result of the compilation is a set of static html pages where graphical themes, menus and hyperlinks are managed automatically. The site is called static for it does not rely on any server-side scripts (such as PHP) nor any database (no MySQL need to be installed on the server), it is merely composed of html pages together with css files for layout design.

The advantages of using a site generator include:

* one can add content directly in markdown files without struggling with html/css tags/attributes, nor with heavy CMS dashboards,
* the website can be easily backed up on a forge such as gitlab.inria and restored / moved to some other location,
* the content is separated from the layout so that contributors do not have to learn the inner structure of the website.

Pelican comes with a large number of plugins (e.g. to manage bibtex references, to include javascript code in templates, etc) and graphical themes.

Among available themes, we chose [jojo](https://github.com/dokelung/jojo). Like [pelican](http://docs.getpelican.com/en/stable/), it is extensively [documented](https://github.com/dokelung/jojo) and open-source. We can thus extend the theme if needed.

## <a name="quickstart"></a>Quickstart 

To contribute to the Synalp website, all that is needed is:
1. an access to the website repository on gitlab.inria.fr (which should be ok if you see this)
2. to have installed the pelican tools (mostly written in python), namely:
   * [pelican](https://github.com/getpelican/pelican)
   * [pybtex](https://pybtex.org/)

Once this is done, you can clone the team website from its git repository via:
<pre>
git clone git@gitlab.inria.fr:synalp/website.git
</pre>

### <a name="archi"></a>Architecture of the Synalp's website repository 

The structure of the website source code is the following:

<pre>
 pelican-j/
 |
 -- pelicanconf.py
 |
 -- pelican-theme/
 |  |
 |  -- jojo
 |     |
 |     -- templates/
 |     |
 |     -- static/
 |
 |-- pelican-plugins/

 |
 -- output/
 |
 -- content/
    |
    --pages/
    |
    --articles/
    |
    --css/
    |
    --js/
    |
    --images/
    |
    --bibtex/
</pre>

At the root of this directory there is a `pelicanconf.py` file which contains customisable information (menus, list of team members, activated plugin, theme options, etc.).

The `pelican-theme` subdirectory contains the definition of the layout of the website. The layout is based on static files (basically css files) and on dynamic files, called templates (written in [jinja](http://jinja.pocoo.org/)).

The `pelican-plugins` subdirectory contains plugins which offer various additional functionalities (such as interaction with bibtex files, inclusion of javascripts in specific web pages, etc).

The `content` subdirectory contains the source code of the website (that is, where the markdown files are stored).

The `output` subdirectory contains the result of the website compilation (which has to be copied to the webserver when the site is published).

### <a name="new"></a>Adding new content 

When one wants to include new content, it can be either a static page (e.g. presentation of the team, list of members, etc) or a blog entry (chronologically sorted post which belongs to the "News" category). Static pages should go to `content/pages/` and blog entries to `content/articles`. In both cases, new content has to be written in [markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) (you can use `.md` as a file extension).

All such files can be edited via any plain-text editor (emacs, gedit, etc). These are made of a header followed by a body.

The header contains metadata (attribute-value pairs such as "Title:blahblah", "Date:2017-08-29 17:20" or "Author:toto", one attribute-value per line) followed by an empty line.
The body then contains plain text (in markdown format).

Images should go to `content/images/` and be referenced from articles or pages via `{filename}/images/filename.ext`. Have a look at existing files for examples.

### <a name="compile"></a>Compiling the website 

To compile the website, you can either use the included `Makefile` (via `make html`) or invoke `pelican content` from the root of the directory.
The resulting webpages will be stored in `output`.

To check the new website before publishing it online, you can do as follows:
<pre>
cd output/
python -m pelican.server
</pre>
It will launch a local webserver, then the website can be displayed locally in your browser at the following address:
<pre>
localhost:8000
</pre>

## <a name="advanced"></a>Advanced features 

Advanced features include:
* using bibtex references
* adding new templates
* changing the website structure
* changing the website URLs

To include bibtex references in your pages or articles, you can use the [pelican-cite plugin](https://github.com/cmacmackin/pelican-cite).

To define a new template (let us call it `toto.html`), you must (i) copy it in `pelican-themes/jojo/templates`.
Then, if you want to use it for a new page you are writing (let us call this page `content/pages/tutu.md`, you have to define in the page's header which template should be called to compile the corresponding html page:
<pre>
Title: The tutu page
author: pierre paul jacques
template: toto
</pre>

Note that metadata defined in a given page or article can be referenced from a template as follows: `page.author`.

New articles will appear direclty in the `News` and in the `[archives]` pages.

New pages must be linked from other static location (menu or other page or article).

To change the structure of the site (that is, the menus), you should edit the `pelicanconf.py` file, it contains plain python code with environment variables (in upper case). These variables can be accessed within templates (that is, the theme's templates or your own templates). In the case of the `jojo` theme, the menus are defined in the `NAV` variable.

We added an `idpage` template to define the members page, it uses the `xxxxx_STAFF` variables from the `pelicanconf.py` file.

## <a name="publish"></a>Publishing the website 

Publishing the website amounts to copying the output directory to the right location on the `alcor` file server.

First, run locally
```
make publish
```

Then, you have several options:
* If you're not in a hurry, you may just push your modifications and wait for someone else to recompile the site; this should be done around once per week
* Or you may `scp -r output/*` onto `youraccount@loria.loria.fr:/local/web-serveurs/synalp/htdocs/` (you need to have an ssh key stored in your homedir on the alcor server for `scp` to work)<sup>[1](#myfootnote1)</sup>
* Or you may mount locally the filesystem `alcor://vol/serveurs/web-serveurs/synalp` and perform the same copy without going through `loria.loria.fr`

## <a name="refs"></a>References

* [markdown cheat sheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Here-Cheatsheet)
* [pelican's documentation](http://docs.getpelican.com/en/stable/)

<a name="myfootnote1">1</a>: To store your ssh key on alcor, you can first mount your homepage from loria or via VPN, and then copy your `.ssh` directory in your homedir.
