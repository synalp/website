
* make loading the website faster by using a smaller thumbnail picture of the members on the front-page
* bugfix: make publish does not work; still have lots of localhost in the output dir !

Added by YP: `make publish` does not work for publishconf.py redefines the variable SITE_URL after importing pelicanconf.py where this variable is used many times (e.g. in the menu items). One solution could be to replace publishconf.py's content with pelicanconf.py's.
