Title: OLKi
slug: olkifr
save_as:pages/olkifr/index.html

# Open Language and Knowledge for Citizens (OLKi)

OLKi est un projet IMPACT de Lorraine Université d'Excellence ([LUE](http://www.univ-lorraine.fr/LUE)) 
focalisé sur des recherches en intelligence artificielle collaboratives et interdisciplinaires.

## Contexte sociétal

Le projet OLKi est d'abord ancré dans un contexte sociétal où s'exprime une inquiétude et une incompréhension grandissante
de la part des citoyens vis-à-vis de l'intelligence artificielle, inquiétude nourrie par des scandales répétés et médiatisés
menant à une méfiance et au risque de rejet de l'IA dans son ensemble. Cette incompréhension s'étend au-delà
de la sphère citoyenne et affecte la communauté scientifique elle-même, qui ne prend pas toujours la mesure de l'importance
des données en particulier langagières, et laisse s'écouler ce flux de richesse hors de nos frontières au seul bénéfice de quelques
acteurs possédant la quasi-totalité des données nourrissant les algorithmes d'IA aujourd'hui.

## Contexte scientifique

A ce contexte sociétal s'ajoute un contexte scientifique marqué par le raz de marée des méthodes de deep learning et de ses applications
en particulier dans le domaine du traitement automatique du langage naturel et de l'intelligence artificielle.
OLKi s'inscrit pleinement dans ce contexte en participant à la conception de nouveaux algorithmes d'apprentissage automatique
dédiés à l'extraction de connaissance à partir des données langagières, mais en mettant l'accent sur la transparence et l'explicabilité des algorithmes,
la science ouverte et respectueuse de la vie privée et l'impact sur les utilisateurs.

## Acteurs

Le projet OLKi est porté par un cercle de cinq laboratoires fondateurs:

- Les Archives Henri Poincaré pour les questions épistémologiques et éthiques
- L'ATILF pour les notions de linguistique
- Le CREM pour les questions d'usage des médias et des réseaux sociaux
- L'IECL pour la formalisation mathématique, notamment concernant l'apprentissage automatique
- Le LORIA pour les aspects informatiques et intelligence artificielle

Le comité opérationnel du projet est ainsi composé de deux membres de chaque laboratoire et animé par Christophe Cerisara, chercheur CNRS au LORIA.
L'interdisciplinarité est un axe central du projet OLKi, qui s'articule au travers d'interactions permanentes et fortes, en particulier entre les sciences
dites dures, que sont les mathématiques et l'informatique, et les sciences humaines et sociales représentées ici par la linguistique, la philosophie et
l'analyse des médias et leurs usages. Cette interdisciplinarité est en effet essentielle pour atteindre les objectifs du projet, notamment l'éthique des
algorithmes d'IA qui implique une analyse épistémologique des procédés scientifiques menant à ces algorithmes, l'usage des données langagières et l'impact
de leur traitement sur les citoyens mettant en oeuvre un dialogue entre chercheurs en usage des médias et en IA, etc.

## Exemples de résultats prévus

Le projet OLKi vise à un changement de paradigme communicationnel, d'abord pour la communauté scientifique elle-même, et à plus long terme pour les citoyens
sur les réseaux sociaux. Afin d'initier ce changement, OLKi développera une nouvelle plateforme de communication alternative au modèle dominant actuel et
qui participera à un mouvement citoyen actuel de grande ampleur car impliquant plus de 2 millions de personnes dénommé le fediverse.
Au-delà des progrès en termes d'éthique, d'ouverture, de transparence et de vie privée, la plateforme fédérée OLKi résoudra de nombreux autres problèmes
des plateformes scientifiques actuelles, dont la maintenance à long terme, la scalabilité, les coûts, le contrôle des fournisseurs de données et
l'interaction recherche/citoyens.
Elle fluidifiera la communication entre les acteurs - chercheurs, fournisseurs de services et citoyens - et hébergera et diffusera des resources scientifiques
liées au langage et aux connaissances qui en sont extraites. Au-delà de ces resources, des avancées scientifiques seront issues des travaux de OLKi
en extraction d'information responsable depuis les données langagières, par exemple
concernant les algorithmes de détection des sentiments ou des discours haineux dans les réseaux sociaux,
la génération automatique de résumés et de définitions textuelles des mots
en contexte pour mieux expliquer le sens d'un document, des outils d'IA pour aider les apprenants du Français langue étrangère à apprendre le français, etc.

### Plus d'information sur [le site web](https://olki.loria.fr)

