Title: AIPROFICIENT
save_as:pages/aiprof/index.html

This project focuses on the deployment of deep learning solutions in the industrial context.
The partners of the project are: LORIA (Synalp), CRAN (leader), CONTINENTAL FRANCE,
FUNDACION TEKNIKER, INEOS SERVICES BELGIUM, TENFORCE BVBA,
TEKNOLOGIAN TUTKIMUSKESKUS VTT OY, INOS HELLAS AE EFARMOGES PLIROFORIKIS AUTOMATISMOU ROMBO,
IBERMATICA SA, INSTITUT MIHAJLO PUPIN, ATHENS TECHNOLOGY CENTER ANONYMI BIOMICHANIKI EMPORIKI KAI TECHNIKI ETAIREIA EFARMOGON YPSILIS TECHNOLOGIAS.

The web site of the project is still in construction.
you may find more information in the following sites and press releases:

- [Cordis portal](https://cordis.europa.eu/project/id/957391)
- [Factuel article](http://factuel.univ-lorraine.fr/node/15927)
- [LORIA article](https://www.loria.fr/en/ai-proficient-artificial-intelligence-for-and-by-humans-at-the-heart-of-the-manufacturing-industry-of-the-future/)
- [CNRS info](https://www.centre-est.cnrs.fr/fr/cnrsinfo/projet-europeen-ai-proficient-lintelligence-artificielle-au-service-de-lhumain-dans)


