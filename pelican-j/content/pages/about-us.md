Title: About Synalp

The SYNALP team has been created in 2012 (as a follow up of the TALARIS project). Its research interests are <b>Natural Language Processing: representations, inference and semantics</b>. It belongs to LORIA's research axis entitled *Knowledge and Languages Management (Department 4)*.

|   |   |   |
|---|---|---|
| _Team leader_  | | Christophe CERISARA |
| _Phone_  | &nbsp; | +33 3 54 95 86 25 |
| _Email_  | | `christophe.cerisara@loria.fr` |

## Presentation ##

The aim of the team is to investigate semantic phenomena (broadly construed) in natural language from a computational perspective. More concretely, Synalp's goal is to develop language models and grammars (with a special emphasis on French) with a semantic dimension; to explore the linguistic and computational issues involved in such areas as natural language generation, textual entailment recognition, discourse and dialogue modeling, and multilinguality; to develop computational tools for inference; and to investigate the interplay between representation and inference in computational semantics for natural language.

The team's research interests also include speech processing, and its relations with NLP, especially at the semantic and syntactic levels, using both symbolic and statistical approaches.

The team's research is applied to various NLP tasks including Man-Machine Dialogue, Language Learning and for Data Verbalisation.

## Research topics ##

* Language Models
* Formal Grammars
* Tools and Resources for Natural Language Processing (NLP)
* Computational Semantics
* Speech Processing

## Collaborations ##

* KIWI team (LORIA, France)
* Multispeech team (LORIA, France)
* Saarland University (Sarrebr&uuml;ck, Germany)

## Keywords ##

Generation, Parsing, Multimedia documents, Text-to-Speech alignment, Sentiment Analysis
