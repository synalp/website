Title: Computers
save_as:pages/computers/index.html

# Ordinateurs de l'equipe

## laptops

- mitra: Prerak; vieux laptop batterie inutilisable
- rameau2: Christophe; laptop en bon etat
- adoma-perso: Jean-Charles
- agni: Yannick
- allegro:
- andiamo: Charlotte; mac-book
- antu: Ale
- apavada: Corinna
- atacama: Jean-Charles
- ayelen: Ale
- biber: Christophe
- cabure: ??
- cairns: Jean-Charles
- chandra: Claire
- chihiro: Yannick
- chloe: Chloe
- chomsky: Samuel
- colin2-perso: Emilie
- cuxacpc: Pascal Cuxac
- cuyen-2: Ale
- dindia: ??
- dindia-old: Christophe
- fay: Christine
- francis: ??
- froberger: Denis
- gallifrey01: Bart
- garuda: Claire
- glorfindel: Samuel
- gugnunk: Claire
- hyperion: Samuel
- impactolki: Aurore
- indra: ??
- jamballa: ??
- kali: Chloé
- linden: Treveur
- lorines: Nadia
- mortimer: ??
- nataraj: Claire
- nlefebvr2: Nicolas
- pampalune: ??
- parvati: Claire
- pushan: Claire
- sapta-perso: ??
- saraswati-hp: Claire
- saraswati01: ??
- syfrap: ??
- tartini: Christophe
- telemann: Christophe
- uranie: Christophe
- walkato: Jean-Charles
- yanislor: Lotfi
- zammih: Emilie

