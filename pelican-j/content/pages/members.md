Title:Members


<b>Synalp in 2015</b>  
<img src="http://synalp.loria.fr/images/photoTeam1.JPG" width="300px"/>

From left to right: Ismael Bada, Nadia Bellalem, Christine Fay-Varnier, Claire Gardent, Bart Lamiroy, Emilie Colin, Laura Perez-Beltrachini, Christophe Cerisara, Samuel Cruz-Lara.

<b>Synalp in 2017</b>  
<img src="http://synalp.loria.fr/images/photoTeam2.jpg" width="300px"/>

From left to right: Valere Richier, Emilie Colin, Yannick Parmentier, Bart Lamiroy, Denis Paperno, Nadia Bellalem, Christine Fay-Varnier, Thien Hoa Le, Bikash Gyawali, Lotfi Bellalem.

<b>Synalp in 2019</b>  
<img src="http://synalp.loria.fr/images/photoTeam3.jpg" width="500px"/>

From left to right: Timothee Mickus, Christophe Cerisara, Nicolas Lefebvre, Emilie Colin, Hubert Nourtel, Pierre-Antoine Rault, Ilias Benjelloun, Claire Gardent, Chloé Braud, Charlotte Roze, Aurore Coince, Hoa Thien Le, Anastasia Shimorina, Anna Liednikova.

<b>Synalp in 2021</b>  
<img src="http://synalp.loria.fr/images/photoTeam4.jpg" width="500px"/>

From left to right: Juliette Faille, Vadim Fomin, Joël Le Grand, Liam Cripwell, Anna Liednikova, Christophe Cerisara, Aurore Coince, Alaeeddine Chaoub, Claire Gardent, Kelvin Han, Jean-Charles Lamirel.


<b>Our offices</b>  

<p>Up to date: Jun 18th 2021</p>

<img src="http://synalp.loria.fr/images/bureaux.svg" width="500px"/>

