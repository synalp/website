Title: SyNaLP (Symbolic and statistical NLP)
slug:presentation
save_as: index.html

Welcome to our website !

The Synalp team is located in [Nancy](http://en.nancy-tourisme.fr), France and is part of the [LORIA](https://www.loria.fr/en) (Laboratoire Lorrain d'Informatique et ses Applications) research unit. It is affiliated to the [French National Scientific Research Center](http://www.cnrs.fr/index.php) (CNRS) and the [Université de Lorraine](http://welcome.univ-lorraine.fr/en/research).

Our research focuses on hybrid, symbolic and statistical approaches to natural language processing and applications built thereon, including NLP for Man-Machine Dialog, for language learning and for Data Verbalisation.

Current research topics include:

* Natural Language Generation
* Human-Machine Dialog
* Named Entity Recognition in Transcriptions of Spoken Text
* Text-to-Speech Alignment
* Semi-supervised Approaches to Syntactic Parsing and Semantic Role Labelling
* Emotion Detection and Modelisation in Tweets
* The investigation and design of advanced weakly supervised machine learning approaches for NLP
* The application of these weakly supervised methods to several NLP tasks, including syntactic and semantic parsing, named entity recognition, coreference resolution, generation, etc. 

Check out information about [our team]({filename}/pages/about-us.md) and our research [projects]({filename}/pages/projects.md).

<br/>

| | | | | |
| :---: | --- | --- | --- | --- |
| ![Logo LORIA]({filename}/images/logo_loria-300x141.jpg) | &nbsp; | ![Logo CNRS]({filename}/images/cnrs.png){: width="140px"} | &nbsp; | ![Logo UL]({filename}/images/ul.png) |
