Title:softwares

Software solutions developed by team members include:

## Generation
---

### [GenI](https://github.com/kowey/GenI/wiki)

GenI is a surface realiser which generates from a flat semantic representation, the sentence(s) verbalising this semantics. GenI uses Feature-Based Lexicalised Tree Adjoining Grammar equipped with a unification based compositional semantics to compute the sentences verbalising the semantic input.

GenI was developed by Carlos Areces and Eric Kow under the supervision of Claire Gardent. It is now maintained by Eric Kow.

To download and install GenI, please visit this [site](https://github.com/kowey/GenI/wiki).

_Contact:_ [Claire GARDENT](mailto:claire.gardent@loria.fr)

### [Jeni](https://bitbucket.org/gardent/jeni)

Jeni is an ongoing port of GenI in Java ([link to current repository](https://bitbucket.org/gardent/jeni)).

_Contact:_ [Claire GARDENT](mailto:claire.gardent@loria.fr)

## Corpus and interactions

### [OLKi](https://olki.loria.fr/platform')

OLKi is a federated data sharing and communication platform that implements the
ActivityPub protocol.

_Contact:_ [Christophe CERISARA](mailto:christophe.cerisara@loria.fr)

## Parsing
---

### [JSafran](https://github.com/cerisara/jsafran)

J-Safran (Java Syntaxico-semantic French Analyser) is a 100%-Java free open-source software for manual, semi-automatic and automatic syntactic dependency parsing in French. It supports other languages as well, but does only include French models.

Demo : [JSafran live demo](http://rapsodis.loria.fr/jsafran/demo.html)

_Contact:_ [Christophe CERISARA](mailto:christophe.cerisara@loria.fr)

## Speech processing
---

### [JTrans](https://github.com/synalp/jtrans)

JTrans is a software that is primarily designed to bring speech alignment (and by extension speech recognition) technology right to the user, and ready to use, in a GUI-rich 100% Java software.

More details on the [jtrans page](https://github.com/synalp/jtrans).

How to cite:

<pre>
@InProceedings{cerisara09,
  author =   {Cerisara, C. and Mella, O. and Fohr, D.},
  title =    {JTrans, an open-source software for semi-automatic text-to-speech alignment},
  booktitle =    {Proc. of INTERSPEECH},
  year =     {2009},
  address =      {Brighton, UK},
  month = sep,
}
</pre>

_Contact:_ [Christophe CERISARA](mailto:cristophe.cerisara@loria.fr)

## Sentiment analysis
---

### [SATI API](http://portal.empathic.eu/?q=products/sati-api)

[SATI API](http://portal.empathic.eu/?q=products/sati-api) (Sentiment Analysis from Textual Information) is a multilingual Web API to analyze emotions and sentiments conveyed by text. Given a text, it returns the sentiment (positive, negative, neutral) or the emotion formatted in EmotionML. See the specification and the Web API access at this url.

_Contact:_ [Samuel CRUZ-LARA](mailto:samuel.cruz-lara@loria.fr)
