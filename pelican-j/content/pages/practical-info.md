Title: Practical information
save_as:pages/practical/index.html

<style type="text/css">
.image-left {
  display: block;
  margin-left: 8px;
  margin-right: 8px;
  float: right;
}
</style>

## <a name"#">About LORIA</a> ##

![Picture]({filename}/images/loria.jpg){: .image-left} SYNALP is located in the LORIA laboratory in Nancy, France. Loria is the French acronym for the “Lorraine Research Laboratory in Computer Science and its Applications” and is a research unit (UMR 7503), common to CNRS, the University of Lorraine and INRIA. This unit was officially created in 1997. <br/><br/>

![Picture]({filename}/images/logo_loria-300x141.jpg){: .image-left} Loria's missions mainly deal with fundamental and applied research in computer sciences. <br/> The lab is a member of the Charles Hermite Federation, which groups the four main research labs in Mathematics, in Information and Communication Sciences and in Control and Automation. Bolstered by the 400 people working in the lab, its scientific work is conducted in 28 teams including 15 common teams with INRIA. LORIA is today one of the biggest research labs in Lorraine. 

## <a name="join_us">How to join us ?</a> ##

### Getting to Nancy ###

Nancy, located about 300km east of Paris, is at 1:30 hours by TGV high speed train from Paris. There are direct trains to Nancy station from Paris-Est. Other TGV trains connect Paris CDG airport and other major French cities with the TGV Lorraine train station (For your convenience, the IATA code for the Nancy Lorraine TGV Station is XZI, indicate that code for a connecting TGV air ticket from your travel agency or airline) , located about 40km from Nancy, which is reached by shuttle bus in about 40 minutes. Other trains reach Nancy from Strasbourg (and Switzerland and the south of Germany) and from Luxembourg and Metz (linking Nancy with Benelux and Germany). Consult the DB Web site for train connections to Nancy from within Europe.

By plane, Nancy is best reached through Paris CDG or Orly airports. From Paris CDG, you can catch a direct train to the TGV Lorraine station and then continue by shuttle bus. From Paris CDG or Orly, you can take the RER B to Gare du Nord, walk or take the Metro 4 to Gare de l’Est and take a TGV to Nancy train station. Luxembourg airport can be convenient as well: take the bus to Luxembourg train station, from which there are frequent connections to Nancy.

Alternatives are Frankfurt or Strasbourg airports. Low-cost flights reach Basel-Mulhouse or Frankfurt-Hahn, but it can be more difficult to reach Nancy from these airports by public transportation. Metz-Nancy regional airport is located close to the TGV Lorraine station and is served by the same shuttle buses.

### Getting to LORIA ###

LORIA is located on the campus of the Faculty of Sciences of Nancy University (Université Henri Poincaré). Its physical address is

 615 rue du Jardin Botanique
 54602 Villers-lès-Nancy
 tel. (+33) 383 59 30 00

From the train station, LORIA is easily reached by bus or by tram.

*Access by tram*: Take the T1 line (line map) at the Nancy Gare stop in the direction of Vandoeuvre CHU and get off at the stop Callot. Then traverse the University campus (see map below): from the tram stop, go right in the direction of Lycée Jacques Callot, then follow the street uphill (turning left), finally enter the campus. LORIA is the white building on the top of the hill. The tram takes less than 15 minutes and runs approximately every 7-15 minutes during working hours.

*Access by bus*: Take the Campus line (also known as 8 – line map) from one of the Nancy centre stops in the direction of Vandoeuvre CHU and get off at the stop Grande Corvée. LORIA is on your left, walk uphill about 30m. (The bus stop is in Rue du Jardin Botanique, facing the back side of LORIA, the main entrance to LORIA is on the other side.) The bus ride takes less than 15 minutes, buses run every 8-15 minutes during working hours.

Like many French cities, Nancy has a system of short-term bike rentals called Velostan. Rates are available here. The bike ride from the city center to LORIA takes about 20 minutes.

![From tram stop to LORIA](https://www.botconf.eu/wp-content/uploads/2014/11/nancyloriatram.png)


## <a name="accomodation">Accomodation</a> ##

Here is a selection of some hotels in Nancy, which are located close to the city center and the tram and bus lines to LORIA. For more information, see the [Tourist Office of Nancy](http://en.nancy-tourisme.fr/stay/accommodation/), or yout preferred online hotel reservation platform.

* [Best Western Hôtel Crystal](http://www.bestwestern-hotel-crystal.com/) (***)
  5 rue Chanzy, 54000 Nancy
  tel: (33) 3 83 17 54 00, fax: (33) 3 83 17 54 30
* [Coeur de City Hotel Nancy Stanislas](http://www.hotel-nancy-stanislas.com/en) (***)
  61 rue Pierre Semard, 54000 Nancy
  tel: (33) 3 83 32 28 53, fax: (33) 3 83 32 79 97
  nancy@coeurdecity.com
* [Hôtel Mercure Nancy Centre Stanislas](http://www.accorhotels.com/fr/hotel-1068-mercure-nancy-centre-stanislas/index.shtml) (***)
  5 rue des Carmes, 54000 Nancy
  tel: (33) 3 83 30 92 60, fax: (33) 3 83 30 92 92
* [Residhome Nancy Lorraine](http://www.residhome.com/uk/hotel-residence-aparthotel-nancy-123.html)
  9 boulevard de la Mothe – 54000 NANCY
  Tel : +33 (0)3 83 19 55 60
  nancy.lorraine@residhome.com
* [Hôtel de Guise](http://www.hoteldeguise.com/) (**)
  18 rue de Guise, 54000 Nancy
  tel: (33) 3 83 32 24 68, fax: (33) 3 83 35 75 63
* [Hôtel Ibis Styles Nancy Centre Gare](http://www.accorhotels.com/fr/hotel-6852-ibis-styles-nancy-centre-gare-ex-all-seasons/index.shtml) (ex all seasons) (**)
  3 rue de l’Armée Patton, 54000 Nancy
  tel: (33) 3 83 40 31 24, fax: (33) 3 83 28 47 78
* [Hôtel Ibis Budget Nancy Centre](http://www.ibis.com/fr/hotel-3684-ibis-budget-nancy-centre/index.shtml)
  Allée du Chanoine Drioton, 54000 Nancy
  tel: (+33) 8 92 68 12 86

## <a name="places_to_see">Places to see in Nancy</a> ##

There are many places worth visiting in Nancy and around. We refer to the [Tourist Office](http://en.nancy-tourisme.fr/) for a more exhaustive presentation of those places, and give here our very short list.

* Nancy is famous around the world for its group of architectural masterpieces inscribed on the UNESCO World Heritage List: [Place Stanislas, Place de la Carrière and Place d'Alliance](http://en.nancy-tourisme.fr/discover/history-and-heritage/unesco/).
* Not far away from Place Stanislas, you can find the [old town](http://en.nancy-tourisme.fr/discover/history-and-heritage/nancy-old-town/).
* Nancy is also famous for its many Art Nouveau houses. A [map with the major Art Nouveau houses can be downloaded from the Tourist Office](http://www.nancy-tourisme.fr/fileadmin/nancy_tourisme/documents/Infos_Pratiques/Brochures_a_telecharger/pdf/art_nouveau_the_itineraries.pdf).
* For a walk in a park, go to the [Pépinière Park](http://en.nancy-tourisme.fr/discover/parks-and-gardens/pepiniere-park/), a large green area a few meters away from Place Stanislas and the old town.
