Title: PAPUD ITEA project
save_as:pages/papud/index.html

### Links of PAPUD resources

- [PAPUD ITEA website](https://itea3.org/project/papud.html)
- [gitlab repo: Esteban's model](https://gitlab.inria.fr/emarquer/papud-bull-nn)
- [gitlab repo: project management data at LORIA](https://gitlab.inria.fr/synalp/papud)

