Title: PhD defense: Hoa Thien Le
Date: 2020-03-29 10:00
Category: team
Tags: phd
Author: Christophe Cerisara

![Photo of Hoa](https://secure.gravatar.com/avatar/8cbc7e212c08cbdd754a5b6d66f1c870?s=100&d=identicon&r=g)

Hoa successfully defended his PhD on Friday morning.

Congratulations Hoa !

You may find [information about the defense here](http://doctorat.univ-lorraine.fr/fr/soutenances/le-thien-hoa).

Hoa will keep on working with us until July.

