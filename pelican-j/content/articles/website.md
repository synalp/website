Title: FAQ Synalp website
Date: 2017-09-15 12:20
Category: team
Tags: website
Author: Christophe Cerisara

Here is a small FAQ about the website:

## Q: How do I add a new post ?

See [this tuto](https://gitlab.inria.fr/synalp/website/blob/master/HOWTO.md#new)

## Q: I've added a post; how do I check it is OK ?

After modifying the web site, it is best practice to check that 
the web site is still compiling before commiting and pushing.

You may compile locally with
```
make html
```
and if there is no error, you may commit and push with:
```
git commit -am "my modifs"
git push origin master
```

## Q: I've added a post; how do I deploy it ?

You have nothing to do: the new website should be deployed automatically (1 or 2 minutes) after every push !

This is achieved thanks to gitlab continuous integration, with a dedicated runner.

If, after 10 minutes, you still don't see your update onto the synalp website, double check that your browser
is not showing you a cached version of the website (force refresh should solve this), and if it is still not
up-to-date, then this may be because compilation failed or the runner crashed: then just drop me (xtof) a line...

## Q: I see something wrong on the website, but I don't have time to fix: what should I do ?

Just report the suggested fix as a gitlab issue:

- login to the INRIA gitlab [here](https://gitlab.inria.fr/synalp/website)
- click on *Issues* and look/search if your remark is not already in there
- click on *New Issue* to add your remark (you can think of *Issues* as a TODO list)

