Title: QUANTUM ANR project accepted
Date: 2019-07-16 12:20
Category: projects
Tags: projects
Author: Claire Gardent

Our proposal for an ANR PRCE Project on Question Generation for Reading Comprehension has been accepted. 

The project consortium brings together two academic partners (IRIT, Toulouse and LORIA, Nancy) and an industrial (Synapse, Toulouse). It aims to develop advanced models of question generation that can improve industrial applications by supporting the automatic construction and enrichment of knowledge bases used within customer service conversational agents, decision support systems, and technical question answering systems in engineering environments.
