Title: TACL Paper accepted  
Date: 2020-09-20 12:20
Category: papers
Tags: papers
Author: Claire Gardent

[Modeling Global and Local Node Contexts for Text Generation from Knowledge Graphs](https://www.mitpressjournals.org/doi/full/10.1162/tacl_a_00332)
Leonardo F. R. Ribeiro, Yue Zhang, Claire Gardent and Iryna Gurevych

