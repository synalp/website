Title: New team website
Date: 2017-08-29 17:20
Category: team
Tags: website
Author: Yannick Parmentier

A first complete version of the new Synalp website is available.

Come to the next team meeting (2017/08/31) for an overview `:-)`

Should be able to be deployed after every push
