Title: PAPUD project accepted
Date: 2017-10-28 12:20
Category: projects
Tags: projects
Author: Christophe Cerisara

The new ITEA3 project [PAPUD](https://itea3.org/project/papud.html) has been accepted and is funded by FUI,
thanks to the efforts of Samuel Cruz-Lara: thank you [@Samuel](https://twitter.com/scruzlara) !

We are thrilled to be part of it and to start working on this challenging topic.
If you have or plan to obtain a Ph.D. in Computer Science, and feel interested by this domain, please send
us your CV.

