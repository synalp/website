Title: How to migrate git repos onto gitlab
Date: 2017-12-01 08:20
Category: team
Tags: git
Author: Christophe Cerisara


It's possible to automatically create a repository on gitlab, without using any web interface, thanks to gitlab API.
In order to use the gitlba API, you must first create a "secret token" that authenticates your script when it connects to gitlab API.
You can create such a token from your profile webpage on gitlab: go to your "settings", and then chooses the menu "Access Tokens".

Once you have a token, you may call the following script to create a repository:

(warning: I've not tested this script yet, I'll do it asap; also you may have to modify it a bit whenever the gitlab API version changes.
So please before running this script with lots of repositories, make a test first with a single toy repo !)

```
#!/bin/bash

# Script to automate the deployment of new private gitlab repos
# TODO: simplify and enhance error handling (already existing repos etc.)
#       expand script to handle: public repos, deletion, re-naming

token="your_token_here"

function usage {
  echo "Usage: $0 -n name"
  exit 1
}

number_args=$#
if [[ !("$number_args" -eq 2) ]]; then
  echo "Incorrect number of args" >&2
  echo "If two strings in name, quote them: e.g. \"str1 str2\""
  usage
fi

while getopts "n:" name; do
  case "${name}" in
    n)
      repo=${OPTARG}
      ;;
    \?)
      usage
      ;;
    *)
      usage
      ;;
  esac
done

if [[ -z ${repo} ]]; then
  echo "Option -n requires an argument." >&2
  usage
else
  echo "#######\nCreating new repo: ${repo}\n#######\n"
fi

# In case space separate words are used for the repo name, the first word will be used
# when naming the json output file.
repo_short=$(echo ${repo} | cut -d " " -f1)

response=$(curl -s -o ./temp.json -w '%{http_code}' \
-H "Content-Type:application/json" https://gitlab.com/api/v3/projects?private_token=$token \
-d "{ \"name\": \"${repo}\" }")

# Format JSON log
cat ./temp.json | python -m json.tool > ./${repo_short}_repo.json
rm -f ./temp.json

echo "Any JSON output is logged in ./${repo_short}_repo.json"
if [ $response != 201 ]; then
  echo "Error\nResponse code: $response"
  exit 1
else
  echo "Repo successfully created\nResponse code: $response"
  exit 0
fi
```

So if you want to migrate multiple repositories onto gitlab, you basically need to
write a script that:

- calls the above script to create the target repository (beware that the repo name does not already exist !)
- within your local git repository, adds the corresponding remote with
```
git remote add newrepo git@gitlab.inria.fr:user/repo.git
```
- then pushes your local git repo onto gitlab
```
git push newrepo --all
```
- iterates to the next repository


