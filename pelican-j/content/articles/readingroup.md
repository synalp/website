Title: Synalp reading group
Date: 2019-03-17 12:20
Category: research
Tags: research
Author: Christophe Cerisara

A Synalp reading group is organized every two weeks; you can find more information about it [here](https://timotheemickus.github.io/reading_group).


