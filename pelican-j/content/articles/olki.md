Title: OLKi project accepted
Date: 2018-10-17 12:20
Category: projects
Tags: projects
Author: Christophe Cerisara

The new LUE IMPACT project [OLKi](https://olki.loria.fr) has been accepted and is funded by Lorraine Université d'Excellence (LUE) !

This is a great opportunity for us to develop transparent and ethical AI researches in collaboration with our academic partners.

