Title: One paper accepted at COLING 2020
Date: 2020-09-30 12:20
Category: papers
Tags: papers
Author: Claire Gardent

Paper accepted at [COLING 2020](https://coling2020.org/)

"Learning Health-Bots from Training Data that was Automatically Created using Paraphrase Detection and Expert Knowledge"
Anna Liednikova, Philippe Jolivet, Philippe Jolivet, Alexandre Durand-Salmon and
Claire Gardent

Creating training data and learning a chatbot to model patient-doctor interactions in the context of clinical studies.