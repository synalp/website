Title: XNLG AI Chair to start on 01/09/2020
Date: 2020-03-25 09:20
Category: projects
Tags: projects
Author: Claire Gardent

Our proposal for an [AI Research and Teaching Chair](https://anr.fr/en/call-for-proposals-details/call/research-and-teaching-chairs-in-artificial-intelligence/) on Multi-lingual, Multi-source Natural Language Generation (XNLG) has been accepted.

Co-funded by the French ANR, Facebook AI Research (Paris) and the Région Grand Est, XNLG will recruit three PhD students and a postdoc to work with Claire Gardent (PI) and the Synalp Team. Strong interaction are in particular expected with the H2020 Innovative Training Network NL4XAI onInteractive  Natural Language Technology for Explainable AI and the ANR Project QUANTUM on Question Generation. 