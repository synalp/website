Title: Welcome to our new members !
Date: 2017-10-22 12:20
Category: team
Tags: members
Author: Christophe Cerisara

<a href="http://www.loria.fr/fr/bienvenue-a-chloe-braud/">LORIA news</a>

After welcoming *Denis Paperno* and *Bart Lamiroy* a few months ago,
we are very glad to welcome *Chloé Braud* and *Yannick Parmentier* as new permanent members in our team !

Welcome ! 

