Title: Juliette Faille joined us to start a PhD within the H2020 NL4XAI Innovative Training Network. Welcome!
Date: 2020-09-08 09:20
Category: projects
Tags: projects
Author: Claire Gardent

Funded by the H2020 program of the European Union, the NL4XAI ITN  will train 11 creative, entrepreneurial and innovative early-stage researchers (ESRs), who will face the challenge of making AI self-explanatory and thus contributing to translate knowledge into products and services for economic and social benefit, with the support of Explainable AI (XAI) systems.

Within SYNALP, Juliette will work on explainable Natural Language Generation models. 

