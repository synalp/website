Title: WebNLG+ Challenge
Date: 2020-12-02 12:20
Category: research
Tags: research
Author: Claire Gardent

The results of the WebNLG+ Challenges are out

Please [go there](https://webnlg-challenge.loria.fr/challenge_2020/#challenge-results) for more details !


