Title: NL4XAI H2020 Innovation Training Network (ITN) project accepted
Date: 2019-05-19 12:20
Category: projects
Tags: projects
Author: Claire Gardent

Our proposal for a European Innovative Training Network (H2020 ITN) on Interactive  Natural Language Technology for Explainable AI has been accepted.

Funded by the H2020 program of the European Union, the NL4XAI ITN  will train 11 creative, entrepreneurial and innovative early-stage researchers (ESRs), who will face the challenge of making AI self-explanatory and thus contributing to translate knowledge into products and services for economic and social benefit, with the support of Explainable AI (XAI) systems.

NL4XAI focuses on the automatic generation of interactive explanations in natural language (NL), as humans naturally do, and as a complement to visualization tools.

The NL4XAI consortium gathers:

 - CiTIUS-USC (PI, Spain) (Contact: Jose M. Alonso)
 - Univ. of Aberdeen (UK) (Contact: Ehud Reiter)
 - Univ. of Malta (Malta) (Contact: Albert Gatt)
 - Univ. of Utrecht (The Netherlands) (Contact: Kees van Deemter)
 - Univ. of Twente (The Netherlands) (Contact: Mariët Theune)
 - CNRS (France) (Contact: Claire Gardent)
 - IIIA-CSIS (Spain) (Contact: Ramón López de Mántaras)
