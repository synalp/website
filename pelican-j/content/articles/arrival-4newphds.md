Title: Kelvin, Liam, Teven and Vadim us to start a PhD. Welcome!
Date: 2020-10-05 09:20
Category: team
Tags: phd
Author: Claire Gardent

Kelvin Han is funded by the ANR QUANTUM project, Liam Cripwell and Vadim Fomin are by the XNLG AI Chair and Teven Le Scao by Hugging Face. 