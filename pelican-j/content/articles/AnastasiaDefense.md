Title: PhD defense: Anastasia Shimorina
Date: 2021-02-27 14:00
Category: team
Tags: phd
Author: Christophe Cerisara

Anastasia successfully defended her PhD on Friday afternoon.

Congratulations Anastasia!

You may find [information about the defense here](http://doctorat.univ-lorraine.fr/fr/soutenances/shimorina-anastasia).


