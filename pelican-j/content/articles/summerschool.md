Title: Summer school in NLP
Date: 2019-04-17 12:20
Category: research
Tags: research
Author: Christophe Cerisara

We organize a summer school about python programming for Natural Language Processing, in Nancy, in the last week of August.

Please [go there](https://synalp.loria.fr/python4nlp/) for more details !


