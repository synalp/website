Title: PhD defense: Emilie Colin
Date: 2020-06-25 10:00
Category: team
Tags: phd
Author: Christophe Cerisara

![Photo of Emilie](https://members.loria.fr/EColin/wp-content/blogs.dir/132/files/sites/132/2015/06/id.png)

Emilie successfully defended his PhD on Thursday.

Congratulations Emilie !

You may find [information about the defense here](http://doctorat.univ-lorraine.fr/fr/soutenances/colin-emilie).

