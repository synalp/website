Title: One paper accepted at EMNLP 2020
Date: 2020-09-20 12:20
Category: papers
Tags: papers
Author: Claire Gardent

Paper accepted at [EMNLP 2020](https://2020.emnlp.org/)

"Multilingual AMR-to-Text Generation"
Angela Fan and Claire Gardent

How to generate from English-centric AMRs into Bulgarian, Czech, Danish, Dutch, English, German, Greek, Spanish, Estonian, Finnish, French, Hungarian, Italian, Lithuanian, Latvian, Polish, Portuguese, Romanian, Slovak, Slovenian, and Swedish.