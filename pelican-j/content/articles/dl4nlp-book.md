Title: Book on Neural NLG out!
Date: 2020-03-25 10:00
Category: papers
Tags: papers
Author: Claire Gardent

Our introductory book on neural NLG is finally out.      
[Deep Learning Approaches to Text Production](http://www.morganclaypoolpublishers.com/catalog_Orig/product_info.php?products_id=1520)    
Shashi Narayan, University of Edinburgh   
Claire Gardent, CNRS/LORIA, Nancy   