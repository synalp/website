#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
import os

AUTHOR = u'Synalp Team'
SITENAME = u'Synalp'
#SITEURL = 'http://localhost:8000'
SITEURL = 'https://synalp.loria.fr'

PATH = 'content'
TIMEZONE = 'Europe/Paris'
DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = 'feeds/all.atom.xml'
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
# LINKS = (('Pelican', 'http://getpelican.com/'),
#          ('Python.org', 'http://python.org/'),
#          ('Jinja2', 'http://jinja.pocoo.org/'),
#          ('You can modify those links in your config file', '#'),)

# Social widget
# SOCIAL = (('You can add links in your config file', '#'),
#           ('Another social link', '#'),)

DEFAULT_PAGINATION = 10
DELETE_OUTPUT_DIRECTORY = True

DEFAULT_CATEGORY = 'Misc'
PAGE_PATHS = ['pages']

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

DIRECT_TEMPLATES = ('index', 'tags', 'categories', 'archives', 'search')

#THEME RELATED
THEME = 'pelican-themes/jojo'

# content path
PATH = 'content'

# specify plugins
PLUGIN_PATHS = ['pelican-plugins']
PLUGINS = ['tipue_search', 'just_table', 'representative_image', 'render_math', 'pelican-cite', 'pelican-bibtex', 'pelican_javascript',]

MATH_JAX = {'color':'blue','align':'left'}

# just table settings
JTABLE_TEMPLATE = """
<table class="uk-table uk-table-striped">
    {% if caption %}
    <caption> {{ caption }} </caption>
    {% endif %}
    {% if th != 0 %}
    <thead>
    <tr>
        {% if ai == 1 %}
        <th> No. </th>
        {% endif %}
        {% for head in heads %}
        <th>{{ head }}</th>
        {% endfor %}
    </tr>
    </thead>
    {% endif %}
    <tbody>
        {% for body in bodies %}
        <tr>
            {% if ai == 1 %}
            <td> {{ loop.index }} </td>
            {% endif %}
            {% for entry in body %}
            <td>{{ entry }}</td>
            {% endfor %}
        </tr>
        {% endfor %}
    </tbody>
</table>
"""

# static
STATIC_PATHS = ['images', 'articles', 'pages', 'bibtex', 'extra']
EXTRA_PATH_METADATA = {
    'extra/favicon.ico': {'path': 'favicon.ico'}
}

# articles
ARTICLE_PATHS = ['articles']
ARTICLE_URL = 'category/{category}/{slug}/'
ARTICLE_SAVE_AS = 'category/{category}/{slug}/index.html'

# pages
PAGE_URL = 'pages/{slug}/'
PAGE_SAVE_AS = 'pages/{slug}/index.html'

SOCIAL = {
    'style': {
        'size': 'medium', # small, medium, large
        'hover': True,    # True, False
        'button': False,  # True, False
    },
    # for SOCIAL, jojo supports uk-icon in uikit2
    # but jojo only recover following icons' color
    'icons': (
        ('envelope-square', 'mailto:christophe.cerisara@loria.fr'),     #
        # ('facebook-square', '#'),   #
        # ('github-square', '#'),     #
        # ('google-plus-square', '#'),#
        # ('linkedin-square', '#'),
        # ('skype', '#'),
        ('twitter-square', 'https://mastodon.etalab.gouv.fr/@cerisara'),
        # ('weixin', '#'),
    )
}

AUTHOR_INFO = {
    'id': AUTHOR,
    'photo': 'photoTeam3.jpg',
    'intro_keywords': (
        ('a Universit&eacute; de Lorraine / CNRS research team', 'http://welcome.univ-lorraine.fr/en/research'),
        ('working on NLP', 'https://en.wikipedia.org/wiki/Natural_language_processing'),
        ('part of LORIA', 'http://www.loria.fr/en/'),        
    ),
    'intro': [
        '',
    ],
    'url': os.path.join(SITEURL, 'pages', 'about-synalp'),
    'social': SOCIAL,
}

NEWEST_ARTICLES = 10  # set 0 to hide this panel

# SIMPLE_PANELS = (
#     {
#         'badge': {
#             'string': 'CNRS',
#             # type can be specified as '' or 'success' or 'warning' or 'danger'
#             # by default, '' is blue, 'success' is green, 'warning' is orange and 'danger' is red
#             # please reference to uikit2
#             'type': '',
#         },
#         'title': '',
#         'photo': 'cnrs1.jpg',
#         'content': 'Affiliated to the French National Scientific Research Center',
#         'link': ('More', 'http://www.cnrs.fr/index.php'),
#     },
#     {
#         'badge': {
#             'string': 'UL',
#             # type can be specified as '' or 'success' or 'warning' or 'danger'
#             # by default, '' is blue, 'success' is green, 'warning' is orange and 'danger' is red
#             # please reference to uikit2
#             'type': '',
#         },
#         'title': '',
#         'photo': 'ul.png',
#         'content': 'Affiliated to Universit&eacute; de Lorraine',
#         'link': ('More', 'http://welcome.univ-lorraine.fr/en/research'),
#     },
# )

RELATED_LINKS = (
    ('LORIA','https://www.loria.fr'),
    ('CNRS','https://www.cnrs.fr'),
    ('Université de Lorraine','https://www.univ-lorraine.fr'),    
    ('Pelican', 'http://getpelican.com/'),
    ('Python.org', 'http://python.org/'),
    ('Jinja2', 'http://jinja.pocoo.org/'),
    ('mg', 'https://github.com/lucachr/pelican-mg'),
)

SHARE_BUTTONS = False
CONTROL_BUTTONS = False

INDEX_SAVE_AS = 'blog/index.html'

NAV = {
    'sitename': SITENAME,
    'navitems': (
        {
            'primary': ('About', AUTHOR_INFO['url']),
            'secondary': (
                {'link':('About Synalp', AUTHOR_INFO['url']) },
                {'type':'divider'},                
                {'type':'header', 'name':'Practical information'},
                {'link':('About LORIA', os.path.join(SITEURL, 'pages', 'practical', 'index.html')) },                
                {'link':('How to join us?', os.path.join(SITEURL, 'pages', 'practical', 'index.html#join_us')) },
                {'link':('Accomodation', os.path.join(SITEURL, 'pages', 'practical', 'index.html#accomodation')) },
                {'link':('Places to see in Nancy', os.path.join(SITEURL, 'pages', 'practical', 'index.html#places_to_see')) },
                )
        },
        {
            'primary': ('Members', os.path.join(SITEURL, 'pages', 'members')),
            'secondary': (
                {'type':'header', 'name':'Permanent staff'},
                {'link':('Team assistants', os.path.join(SITEURL, 'pages', 'staff', 'index.html#admin_staff')) },
                {'link':('Research scientists', os.path.join(SITEURL, 'pages', 'staff', 'index.html#research_staff')) },
                {'link':('University faculty', os.path.join(SITEURL, 'pages', 'staff', 'index.html#university_staff')) },
                {'type':'divider'},
                {'type':'header', 'name':'Temporary staff'},                
                {'link':('PhD students', os.path.join(SITEURL, 'pages', 'staff', 'index.html#phd'))},
                {'link':('Post-doc or engineers', os.path.join(SITEURL, 'pages', 'staff', 'index.html#postdoc'))},
                {'link':('Invited researchers', os.path.join(SITEURL, 'pages', 'staff', 'index.html#invited'))},
                {'link':('External collaborators', os.path.join(SITEURL, 'pages', 'staff', 'index.html#external'))},
                {'link':('Alumni', os.path.join(SITEURL, 'pages', 'staff', 'index.html#alumni'))},                
            )        
        },
        {
            'primary': ('News', os.path.join(SITEURL, INDEX_SAVE_AS)),
        },
        {
            'primary': ('Projects', os.path.join(SITEURL, 'pages', 'projects')),
        },
        {
            'primary': ('Useful links', os.path.join(SITEURL, 'pages', 'useful.html')),
        },
        {
            'primary': ('Publications', os.path.join(SITEURL, 'pages', 'publications.html')),
            'secondary': (
                {'link':('By year', os.path.join(SITEURL, 'pages', 'publications.html')) },
                {'link':('By type', os.path.join(SITEURL, 'pages', 'pubtype.html')) },
                )
        },
        {
            'primary': ('Softwares', os.path.join(SITEURL, 'pages', 'softwares')),
            'secondary': (
                {'type':'header', 'name':'Corpus/Interactions'},
                {'link':('OLKi', 'https://olki.loria.fr/platform') },
                {'type':'header', 'name':'Generation'},
                {'link':('GenI', 'https://github.com/kowey/GenI/wiki') },
                {'link':('JenI', 'https://bitbucket.org/gardent/jeni') },
                {'type':'header', 'name':'Parsing'},
                {'link':('JSafran', 'https://github.com/cerisara/jsafran') },
                {'type':'header', 'name':'Speech Processing'},
                {'link':('JTrans', 'https://github.com/synalp/jtrans') },
                {'type':'header', 'name':'Sentiment Analysis'},
                {'link':('SATI', 'http://portal.empathic.eu/?q=products/sati-api') },
                )
        },
        {
            'primary': ('[archives]', os.path.join(SITEURL, 'archives.html')),
        },                    
    ),
    'tipue_search': True,
}

LOCATION = True

FOOTER = {
    'year': 2017,
    'author': AUTHOR,
    'license': {
        'name': 'The MIT License',
        'link': 'https://opensource.org/licenses/MIT',
    }
}

#For publication ordering
PUBLICATIONS_SRC = 'content/bibtex/SYNALP.bib'

## FOR MULTIPLE SORT CRITERIA (PUBTYPE)
import operator

def sort_multi(L,rev=False,*operators): 
    L.sort(key=operator.itemgetter(*operators), reverse=rev)
    return L

def sort_pub(L, ptype, pyear):
    L.sort(key=operator.itemgetter(pyear),reverse=True)
    L.sort(key=operator.itemgetter(ptype))
    return L

JINJA_FILTERS = {'sort_multi':sort_multi, 'sort_pub':sort_pub}

#SYNALP SPECIFIC
ADMIN_STAFF = (
    { 'name': 'Delphine HUBERT',
      'email': 'delphine.hubert at loria dot fr',
      'phone': '+33 3 83 59 20 59',
      'office': 'B272',
    },
    { 'name': 'Anne-Marie Messaoudi',
      'email': 'anne-marie.messaoudi at loria dot fr',
      'phone': '+33 3 83 59 30 51',
      'office': 'B272',
    },
)

RESEARCH_STAFF = (
    { 'name': 'Christophe CERISARA',
      'email': 'christophe.cerisara at loria dot fr',
      'phone': '+33 3 83 59 ',
      'office': 'B114',
      'www'  : 'https://members.loria.fr/CCerisara/',
      'picture' : 'https://members.loria.fr/CCerisara/images/moi2016.png',
    },
    { 'name': 'Claire GARDENT',
      'email': 'claire.gardent at loria dot fr',
      'phone': '+33 3 83 59 20 39',
      'office': 'B114',
      'www' : 'https://members.loria.fr/CGardent/',
      'picture' : 'http://www.maisons-pour-la-science.org/sites/default/files/upload/Claire%20Gardent%20portrait.png',
    },
)

UNIVERSITY_STAFF = (

        { 'name': 'Lotfi BELLALEM',
          'email': 'lotfi.bellalem at loria dot fr', 
          'office': 'B114',
          'phone' : '+33 (0)3 83 59 20 31',          
        },
        { 'name': 'Nadia BELLALEM',
          'email': 'nadia.bellalem at loria dot fr',               
          'office': 'B112',
          'phone' : '+33 (0)3 83 59 20 31',
          'picture': os.path.join(SITEURL, 'images', 'nadia.jpg'),
        },
        { 'name': 'Samuel CRUZ-LARA',
          'email': 'samuel.cruz-lara at loria dot fr',               
          'office': 'B108',
          'phone' : '+33 (0)3 83 59 20 31',
          'www'   : 'https://www.linkedin.com/in/samuelcruzlara/?ppe=1',
          'picture' : 'https://media.licdn.com/media/p/5/000/234/0f2/0da103b.jpg',
        },
        { 'name': 'Christine FAY-VARNIER',
          'email': 'christine.fay at loria dot fr',               
          'office': 'B110',
          'phone' : '+33 (0)3 83 59 20 01',
          'picture' : 'https://i1.rgstatic.net/ii/profile.image/AS%3A487406420926469%401493218154591_l/Christine_Fay-Varnier.png',
        },
        { 'name': 'Jean-Charles LAMIREL',
          'email': 'jean-charles.lamirel at loria dot fr',               
          'office': 'B104',
          'phone' : '+33 (0)3 54 95 85 67',
          'picture' : 'https://i1.rgstatic.net/ii/profile.image/AS%3A279129766285314%401443561130284_l/J-C_Lamirel.png',
          'www' : 'https://www.researchgate.net/profile/J-C_Lamirel',
        },
        { 'name': 'Joel LEGRAND',
          'email': 'joel.legrand at loria dot fr',               
          'office': 'B104',
          'phone' : '+33 (0)3 54 95 85 67',
        },
        { 'name': 'Yannick PARMENTIER',
          'office': 'B110',
           'email': 'yannick.parmentier at loria dot fr',
           'phone': '+33 3 54 95 86 58',
           'www'  : 'https://sourcesup.renater.fr/tulipa/yannick',
           'picture': 'https://annuaire.univ-orleans.fr/photo/index/c_uid/p15816',
        },
)

PHD = (
    { 'name': 'Ilias Benjelloun',
      'email': 'ilia.benjelloun at loria dot fr',
      'phone' : '+ 33 3 54 95 84 80',
      'office': 'B108 (co-supervised with IECL)',
    },
    { 'name': 'Guillaume Le Berre',
      'email': 'guillaume.leberre at loria dot fr',
      'phone' : '+ 33 3 54 95 84 80',
      'office': 'B108 (co-supervised with University of Montreal)',
    },
    { 'name': 'Paul Caillon',
      'email': 'paul.caillon at loria dot fr',
      'phone': '+33 3 83 59 20 00',
      'office': 'B105',
    },
    { 'name': 'Alaaeddine Chaoub',
      'email': 'alaaeddine.chaoub at loria dot fr',
      'phone': '+33 3 83 59 20 00',
      'office': 'B105',
    },
    { 'name': 'Liam Cripwell',
      'email': 'liam.cripwell at loria dot fr',
      'phone': '+33 3 83 59 20 00',
      'office': 'B105',
    },
    { 'name': 'Juliette Faille',
      'email': 'juliette.faille at loria dot fr',
      'phone': '+33 3 83 59 20 00',
      'office': 'B106 (Funded by NL4XAI; Co-supervisor: Albert Gatt, University of Malta)',
      'www' : 'https://www.linkedin.com/in/juliette-faille-b6212110b/?originalSubdomain=fr',
    },
    { 'name': 'Angela Fan',
      'email': 'angela.fan at loria dot fr',
      'phone': '+33 3 83 59 20 00',
      'office': 'B106 (Funded by Facebook AI Research Paris; Co-supervisor: Antoine Bordes)',
      'www' : 'https://research.fb.com/people/fan-angela/',
    },
    { 'name': 'Vadim Fomin',
      'email': 'vadim.fomin at loria dot fr',
      'phone': '+33 3 83 59 20 00',
      'office': 'B106 (Funded by XNLG)',
      'www' : 'https://www.linkedin.com/in/vadim-fomin-b40814189/',
    },
    { 'name': 'Kelvin Han',
      'email': 'kelvin.han at loria dot fr',
      'phone': '+33 3 83 59 20 00',
      'office': 'B106 (Funded by ANR QUANTUM)',
      'www' : 'https://www.linkedin.com/in/kelvin-han/',
    },
    { 'name': 'Anna Liednikova',
      'email': 'anna.liednikova at loria dot fr',
      'phone': '+33 3 83 59 20 00',
      'office': 'B108 (Funded by ALIAE)',
      'www' : 'https://www.linkedin.com/in/anna-liednikova/',
    },
    { 'name': 'Anastasia Shimorina',
      'email': 'anastasia.shimorina at loria dot fr',
      'phone': '+33 3 83 59 20 34',
      'office': 'B106 (Funded by the French Governement)',
    },
    { 'name': 'Teven Le Scao',
      'email': 'teven.le-scao at inria dot fr',
      'phone': '+33 3 83 59 20 34',
      'office': 'B106 (Funded by HuggingFace)',
    },
)

POSTDOC = (
)

#INVITED = (
#    { 'name': 'Jacques DUCLOY',
#      'email': 'jacques.ducloy at loria dot fr',
#      'phone': '+33 6 85 75 82 67',
#      'office': 'B215',
#      'www' : 'http://ticri.univ-lorraine.fr/wicri-lor.fr/index.php/Utilisateur:Jacques_Ducloy',
#      'picture': 'https://0.academia-photos.com/29018629/8261452/9241657/s200_jacques.ducloy.jpg_oh_138e1f291c870e43360ef9a39008c8de_oe_55b1f184___gda___1437038029_90fdbe9e93acce194bf0051ebcc62f65',
#    },
#)

EXTERNAL = (
    { 'name': 'Pavel KRAL',
      'email': 'pkral at kiv dot zcu dot cz ',
      'phone': '+420 377 632 454',
      'office': '',
      'picture' : 'https://home.zcu.cz/~pkral/pavel.jpg',
      'www' : 'https://home.zcu.cz/~pkral/',
    },
    { 'name': 'Bart Lamiroy',
      'email': 'Bart.Lamiroy@univ-reims.fr',
      'phone': '',
      'office': '',
      'picture' : 'https://crestic.univ-reims.fr/uploads/membresCrestic/5f7f11c34d840.jpeg',
      'www' : 'https://crestic.univ-reims.fr/fr/lamiroy',
    },

)

PrivateALUMNI = (
    { 'name': 'Chloé Braud',
      'position': 'Researcher at IRIT',
    },
    { 'name': 'Charlotte Roze',
      'position': 'Private',
    },
    { 'name': 'Victor Yon',
      'position': 'PhD at LORIA',
    },
    { 'name': 'Nicolas Lefebvre',
      'position': 'Private',
    },
    { 'name': 'Thimothée Mickus',
      'position': 'PhD at ATILF',
    },
    { 'name': 'Denis Paperno',
      'position': 'Univ. Utrecht',
    },
    { 'name': 'Alejandra Lorenzo',
      'position': 'Private',
    },
    { 'name': 'Clement Domps',
      'position': 'Private',
    },
    { 'name': 'Wei Du',
      'position': 'Private',
    },
    { 'name': 'Bikash Gyawali',
      'position': 'Open University, UK',
    },
    { 'name': 'Chunyang Xiao',
      'position': 'Bloomberg LP, UK',
    },    
    { 'name': 'Ismael Bada',
      'position': 'Grid5000, LORIA',
    },
    { 'name': 'Paul Bédaride',
      'position': 'Xylopic, Epinal',
      'www' : 'http://ptitpoulpe.fr/',
    },
    { 'name': 'Alexandre Denis',
      'position': 'SESAMm, Metz',
      'www' : 'https://fr.linkedin.com/in/alexandredenis',
    },
    { 'name': 'Nicolas Dugué',
      'www': 'http://www-lium.univ-lemans.fr/~dugue/',
      'position': 'LIUM, Le Mans',
    },
    { 'name': 'Ingrid Falk',
      'www': 'https://sites.google.com/site/ingridfalkgustedt/',
      'position': 'LiLPa, Universit&eacute; de Strasbourg',
    },
    { 'name': 'German Kruszewski',
      'position': 'University of Trento',
      'www' : 'http://germank.github.io/',
    },
    { 'name': 'Andres Luna',
      'position': 'Argentina',
    },          
    { 'name': 'Mariem Mahfoudh',
      'position': 'Tunisia',
      'www' : 'http://mariem-mahfoudh.info/',
    },
    { 'name': 'Yassine Mrabet',
      'position': 'National Library of Medicine, USA',
      'www' : 'https://lhncbc.nlm.nih.gov/personnel/yassine-mrabet',
    },
    { 'name': 'Shashi Narayan',
      'position': 'Google London, UK',
      'www' : 'http://homepages.inf.ed.ac.uk/snaraya2/',
    },          
    { 'name': 'Lina Mar&iacute;a Rojas Barahona',
      'position': 'Orange Labs, Lannion',
      'www' : 'http://mi.eng.cam.ac.uk/~lmr46/',
    },
    { 'name': 'Laura Perez-Beltrachini',
      'position': 'University of Edinburgh',
      'www' : 'https://www.inf.ed.ac.uk/people/staff/Laura_Perez.html',
    },
    { 'name': 'Guillaume Serriere',
      'position': 'industry',
    },
    { 'name': 'Kohei Yamamoto',
      'position': 'University of Tokyo',
      'www' : 'http://corpy.info/',
    },    
)

